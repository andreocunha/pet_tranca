//funcao para ler uma digital e verificar se ela esta registrada
//a funcao tambem aciona a porta

int getFingerprintIDez() {
  uint8_t p = finger.getImage();
  if (p != FINGERPRINT_OK){
    return -1;
  }

  p = finger.image2Tz();
  if (p != FINGERPRINT_OK) {
    return -1;
  }

  p = finger.fingerFastSearch();
  if (p != FINGERPRINT_OK) {
    digitalWrite(pinoLedVermelho, HIGH);
    oled.clear();
    oled.println("");
    oled.set2X();
    oled.println("NEGADO!");
    delay(2000); //INTERVALO DE 2 SEGUNDOS
    oled.clear();
    digitalWrite(pinoLedVermelho, LOW);
    return -1;
  }
  
  //Encontrou uma digital!
   else {
    
      digitalWrite(pinoLedVerde, HIGH);
     Serial.print(F("ID encontrado #")); Serial.println(finger.fingerID); 

      oled.clear();
      oled.println("");
      oled.set2X();
   

     if(finger.fingerID == 0 || finger.fingerID == 1){
      Serial.println(F("Andre"));
      oled.println("Andre Cunha");
     }

     else if(finger.fingerID == 2 || finger.fingerID == 3){
      Serial.println(F("Marco"));
      oled.println("Marco");
     }

     else if(finger.fingerID == 4 || finger.fingerID == 5){
      Serial.println(F("Victor"));
      oled.println("Victor");
     }

     else if(finger.fingerID == 6 || finger.fingerID == 7){
      Serial.println(F("Caio"));
      oled.println("Caio");
     }

     else if(finger.fingerID == 8 || finger.fingerID == 9){
      Serial.println(F("Mirelly"));
      oled.println("Mirelly");
     }

  else if(finger.fingerID == 10 || finger.fingerID == 11){
      Serial.println(F("Henrique"));
      oled.println("Henrique");
     }

     else if(finger.fingerID == 12 || finger.fingerID == 13){
      Serial.println(F("Elias"));
      oled.println("Elias");
     }

     else if(finger.fingerID == 14 || finger.fingerID == 15){
      Serial.println(F("Joao"));
      oled.println("Joao");
     }

     else if(finger.fingerID == 16 || finger.fingerID == 17){
      Serial.println(F("Luiz"));
      oled.println("Luiz");
     }

     else if(finger.fingerID == 18 || finger.fingerID == 19){
      Serial.println(F("Heitor"));
      oled.println("Heitor");
     }

     else if(finger.fingerID == 20){
      Serial.println(F("Jorge"));
      oled.println("Jorge");
     }

     else if(finger.fingerID == 21 || finger.fingerID == 22){
      Serial.println(F("Lucca"));
      oled.println("Lucca");
     }

     else if(finger.fingerID == 23 || finger.fingerID == 24){
      Serial.println(F("MJ"));
      oled.println("MJ");
     }

     else if(finger.fingerID == 25 || finger.fingerID == 26){
      Serial.println(F("Otto"));
      oled.println("Otto");
     }

     else if(finger.fingerID == 27 || finger.fingerID == 28){
      Serial.println(F("Rodrigo"));
      oled.println("Rodrigo");
     }

     delay(1500);
      oled.clear();
      oled.println("");
      oled.println("Bem vindo!");
      delay(2000);
      oled.clear();
     digitalWrite(pinoLedVerde, LOW);
     return finger.fingerID;
  } 
}
