#include <Adafruit_Fingerprint.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(2, 3); //pinos de leitura do sensor biometrico

Adafruit_Fingerprint finger = Adafruit_Fingerprint(&mySerial);


uint8_t id; //inteiro de 8 bits para o cadastro do id
uint8_t numMenu; //inteiro para as opcoes do menu

const int Botao = 8; //botao no pino 8
int estadoBotao; //Variavel para ler o status do pushbutton


//-------------------------------------------------------------------------------------------------------------------------
//RFID

#include <Wire.h> //INCLUSÃO DA BIBLIOTECA NECESSÁRIA
#include <SPI.h> //INCLUSÃO DE BIBLIOTECA
#include <MFRC522.h> //INCLUSÃO DE BIBLIOTECA

#define SS_PIN 10 //PINO SDA
#define RST_PIN 9 //PINO DE RESET

MFRC522 rfid(SS_PIN, RST_PIN); //PASSAGEM DE PARÂMETROS REFERENTE AOS PINOS


const int pinoLedVerde = 6; //PINO DIGITAL REFERENTE AO LED VERDE
const int pinoLedVermelho = 5; //PINO DIGITAL REFERENTE AO LED VERMELHO



//-------------------------------------------------------------------------------------------------------------------------
//bibliotecas e definicoes para o display OLED

#include "SSD1306Ascii.h"
#include "SSD1306AsciiAvrI2c.h"

// 0X3C+SA0 - 0x3C or 0x3D
#define I2C_ADDRESS 0x3C

// Define proper RST_PIN if required.
#define RST_PIN -1

SSD1306AsciiAvrI2c oled;


//-------------------------------------------------------------------------------------------------------------------------


void setup() {
  Serial.begin(9600); //inicia o monitor serial do arduino, com taxa de 9600
  
  while (!Serial);
  delay(100);

  finger.begin(57600); // defina a taxa de dados para a porta serial do sensor
  
  if (finger.verifyPassword()) {
    Serial.println(F("Sensor biometrico encontrado!"));
  } else {
    Serial.println(F("NAO foi possivel encontrar o sensor biometrico."));
    while (1) { delay(1); }
  }

  Serial.println();
  Serial.println(F("Precione o botao para acessar o MENU"));


  Wire.begin(); //INICIALIZA A BIBLIOTECA WIRE
  SPI.begin(); //INICIALIZA O BARRAMENTO SPI
  rfid.PCD_Init(); //INICIALIZA MFRC522
  
  pinMode(Botao, INPUT); //Pino com botão será entrada
  pinMode(pinoLedVerde, OUTPUT); //DEFINE O PINO COMO SAÍDA
  pinMode(pinoLedVermelho, OUTPUT); //DEFINE O PINO COMO SAÍDA
  
  digitalWrite(pinoLedVerde, LOW); //LED INICIA DESLIGADO
  digitalWrite(pinoLedVermelho, LOW); //LED INICIA DESLIGADO
  

  //-------------------------------------------------------------------------------------------------------------------------
//definicoes inciais para o display OLED
  
#if RST_PIN >= 0
  oled.begin(&Adafruit128x32, I2C_ADDRESS, RST_PIN);
#else // RST_PIN >= 0
  oled.begin(&Adafruit128x32, I2C_ADDRESS);
#endif // RST_PIN >= 0
  // Call oled.setI2cClock(frequency) to change from the default frequency.

  oled.setFont(Adafruit5x7);

  uint32_t m = micros();

  oled.clear();

//-------------------------------------------------------------------------------------------------------------------------

  
}

void loop() {


  //descomentar esse bloco para poder cadastrar/apagar uma digital


 
  estadoBotao = digitalRead(Botao); //le o estado do botão - HIGH OU LOW

  if (estadoBotao == HIGH){
    Serial.println();
    Serial.println(F("MENU:"));
    Serial.println();
    Serial.println(F("Digite 1 para cadastrar nova digital"));
    Serial.println(F("Digite 2 para remover uma digital"));
    Serial.println(F("Digite 3 para ver as digitais cadastradas"));
    Serial.println(F("Digite 4 para sair do MENU"));

    numMenu = readnumber(); // Armazena caractere lido
    Serial.println();

  
    if(numMenu == 1){
      cadastraDigital();
    }

    else if(numMenu == 2){
      apagaDigital();
    }

    else if(numMenu == 3){
      mostraDigitais();
    }

    else if (numMenu == 4){
      Serial.println(F("Saindo..."));  
    }

    Serial.println();
    Serial.println(F("Precione o botao para acessar o MENU"));
    Serial.println();
    }
   
  
 
  getFingerprintIDez(); //funcao para ler a biometria e liberar a trava eletronica
  leituraRfid(); //CHAMA A FUNÇÃO RESPONSÁVEL PELA VALIDAÇÃO DA TAG RFID
 
}
